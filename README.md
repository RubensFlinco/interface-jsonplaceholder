# Dependencias
- Vagrant
- VirtualBox

# Instalação incial
- Você precisa ter instalado o Vagrant & VirtualBox
- Baixar ou ter um clone desse repo
- Entrar na pasta raiz desse repo
- Digitar o comando "vagrant up"
- Pronto só aguardar agora que tudo será instalado na maquina virtual
- Depois de tudo devidamente terminado você pode ver a pagina acessando no seu navegador o endereço http://localhost:8080/

# Quero mexer na maquina virtual como faço?
- Depois de ter feito a instalaçao inical, e estar na pasta raiz desse repo
- Basta digitar "vagrant ssh"
- Pronto você estará dentro do console da maquina virtual

# Quero fechar a maquina virtual como faço ?
- Basta digitar "vagrant suspend"
- Pronto a maquina virtual será destaligada

# Quero deletar a maquina virtual como faço ?
- Basta digitar "vagrant destroy"
- Confirme com "y"
- Pronto a maquina virtual será destaligada e removida

By Rubens Flinco