import React, { Component } from 'react';
import { Link } from 'react-router';
import LayoutMenuCompleto from '../completo/completo';
import LayoutMenuLogoReturn from '../logo-return/logo-return';

class LayoutMenuReduzido extends Component {

  render() {
    return (
      <div>

        <LayoutMenuCompleto />

        <nav className="navbar navbar-gradient navbar-top scrolled navbar-fixed-top">
          <div className="container-fluid">

            <LayoutMenuLogoReturn props={this.props} />

            <div id="navbar" className="navbar-collapse">
              <ul className="nav navbar-right center">
                <li remove-pc="">
                  <Link onClick={LayoutMenuCompleto.prototype.openNav}>
                    <i remove-pc="" className="button fa fa-bars fa-2x fa-mobile-3x" aria-hidden="true"></i>
                    <p>Menu</p>
                  </Link>
                </li>
                <li>
                  <Link to="/">
                    <i remove-pc="" className="button fa fa-home fa-2x fa-mobile-3x" aria-hidden="true"></i>
                    <p>Home</p>
                  </Link>
                </li>
                <li remove-mobile="" className="separar">
                     
                </li>
              </ul>
            </div>

          </div>
        </nav >
      </div>
    )
  }
}
export default LayoutMenuReduzido;

