import React, { Component } from 'react';
import LayoutMenuReduzido from '../../menu/reduzido/reduzido';
import LayourRodape from '../../rodape/rodape';

class LayourNavFooter extends Component {

  render() {
    return (
      <div>
        <LayoutMenuReduzido props={this.props} />
        <div className="espaco-navbar-top"></div>
        <div className="container">
          {this.props.children}
        </div>
        <div className="espaco-top"></div>
        <LayourRodape></LayourRodape>
      </div>
    )
  }
}
export default LayourNavFooter;

