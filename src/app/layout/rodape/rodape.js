import React, { Component } from 'react';

class LayourRodape extends Component {

  render() {
    return (
      <div className="separacao-verde rodape">

        <div className="container">
          <div className="row text-center">
            <div className="col-md-4 col-md-offset-4">
              <h2 className="text-center">Redes Sociais</h2>
              <hr />
              <a target="_blank" rel="noopener noreferrer" href="https://www.linkedin.com/in/rubens-flinco/" className="btn btn-space btn-social btn-color btn-linkedin btn-lg">
                <i className="icon fa fa-linkedin"></i>
              </a>
              <a target="_blank" rel="noopener noreferrer" href="https://www.facebook.com/rubensflinco" className="btn btn-circl btn-social btn-color btn-facebook btn-lg">
                <i
                  className="icon fa fa-facebook"></i>
              </a>
              <a target="_blank" rel="noopener noreferrer" href="https://www.instagram.com/rubensflinco" className="btn btn-space btn-social btn-color btn-instagram btn-lg">
                <i
                  className="icon fa fa-instagram"></i>
              </a>
              <a target="_blank" rel="noopener noreferrer" href="https://twitter.com/JotinhaBrasil" className="btn btn-space btn-social btn-color btn-twitter btn-lg">
                <i className="icon fa fa-twitter"></i>
              </a>
              <a target="_blank" rel="noopener noreferrer" href="http://www.youtube.com/c/MrJotinhaBR" className="btn btn-space btn-social btn-color btn-youtube btn-lg">
                <i className="icon fa fa-youtube-play"></i>
              </a>
            </div>

          </div>
          <div className="row text-center">

            <br /><br />
            <p>© 2019 - By Rubens Flinco
            <br />
              Rubens Flinco | www.rubensflinco.com.br</p>
          </div>
        </div>

      </div>
    )
  }
}
export default LayourRodape;

