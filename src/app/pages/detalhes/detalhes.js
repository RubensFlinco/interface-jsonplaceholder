import { Component } from 'react';
import html from './detalhes.jsx';
import { ServicesUser } from '../../services/user/user.js';

class PageStartups extends Component {
  constructor(props) {
    super(props);
    this.state = {
      json: null,
      erro: null,
      isLoaded: false
    };
  }

  async componentDidMount() {
    window.scrollTo(0, 0);

    const id = this.props.params.id;
    let Users = await ServicesUser.prototype.getUsers();

    Users = Users.filter(function (user) {
      return String(user.id) === String(id);
    });
    Users = Users[0];

    if (!Users) {
      await this.setState({
        isLoaded: true,
        erro: "Nenhum usuario com esse id para detalhar informações sobre ele!"
      });
    }

    await this.setState({
      isLoaded: true,
      Users
    });

    console.log(this.state.Users);
  }

  render() {
    return html(this);
  }
}
export default PageStartups;

