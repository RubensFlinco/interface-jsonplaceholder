import React from 'react';
import LayoutError from '../../layout/error/error';
import LayoutCarregando from '../../layout/carregando/carregando';
import * as Fun from '../../../assets/js/fun';

export default function html(that) {

    if (that.state.erro != null) {
        return (<LayoutError erro={that.state.erro}></LayoutError>);
    } else if (!that.state.isLoaded) {
        return (<LayoutCarregando></LayoutCarregando>);
    } else {

        return (
            <div className="animated fadeIn fast">

                <div className="espaco-6p" remove-mobile="" />

                <div className="col-md-8 col-md-offset-2">
                    <div className="detalhes-info-titulo">
                        <span className="corta animated bounceInLeft">{Fun.titleize(that.state.Users.name)}</span>
                        <span className="pull-right animated bounceInRight">{Fun.titleize(that.state.Users.company.name)}</span>
                    </div>

                    <div className="detalhes-info-corpo">
                        <b>Nome: </b>
                        <span>{that.state.Users.name}</span>
                        <br />
                        <b>Nick: </b>
                        <span>{that.state.Users.username}</span>
                        <br />
                        <b>E-mail: </b>
                        <a target="_blank" rel="noopener noreferrer" href={"mailto:"+that.state.Users.email} >{that.state.Users.email}</a>
                        <br />
                        <b>Telefone: </b>
                        <a target="_blank" rel="noopener noreferrer" href={"tel:"+that.state.Users.phone} >{that.state.Users.phone}</a>
                        <br />
                        <b>Site: </b>
                        <a target="_blank" rel="noopener noreferrer" href={"http://"+that.state.Users.website} >{that.state.Users.website}</a>

                        <hr />

                        <b>Rua: </b>
                        <span>{that.state.Users.address.street}</span>
                        <br />
                        <b>Apartamento: </b>
                        <span>{that.state.Users.address.suite}</span>
                        <br />
                        <b>Cidade: </b>
                        <span>{that.state.Users.address.city}</span>
                        <br />
                        <b>CEP: </b>
                        <span>{that.state.Users.address.zipcode}</span>

                        <hr />

                        <b>Nome da empresa: </b>
                        <span>{that.state.Users.company.name}</span>
                        <br />
                        <b>Slogan 1 da empresa: </b>
                        <span>{that.state.Users.company.catchPhrase}</span>
                        <br />
                        <b>Slogan 2 da empresa: </b>
                        <span>{that.state.Users.company.bs}</span>

                    </div>
                </div>

            </div>
        )

    }
}