import { Component } from 'react';
import html from './home.jsx';
import * as Fun from '../../../assets/js/fun';
import { ServicesUser } from '../../services/user/user.js';
import $ from 'jquery';

class PageHome extends Component {
  constructor(props) {
    super(props);
    this.state = {
      json: null,
      erro: null,
      isLoaded: false
    };
  }

  async componentDidMount() {
    window.scrollTo(0, 0);

    const Users = await ServicesUser.prototype.getUsers();
    await this.setState({
      isLoaded: true,
      Users
    });
    console.log(this.state.Users);
    Fun.AtivarDataTable();
  }

  async filtroEnderecoComSuite(that) {
    let idBtn = "#btn-endereco-com-suite";
    let classBtn = "btn-verde disabled";

    await that.limparFiltros(that);
    await that.setState({
      isLoaded: false
    });
    let filtro = that.state.Users.filter(function (user) {
      let e = user.address.suite.toLowerCase();
      return e.includes('suite') === true;
    });
    await that.setState({
      isLoaded: true,
      Users: filtro
    });
    console.log(that.state.Users);
    $(idBtn).addClass(classBtn);
    Fun.AtivarDataTable();
  }

  async filtroEnderecoSemSuite(that) {
    let idBtn = "#btn-endereco-sem-suite";
    let classBtn = "btn-verde disabled";

    await that.limparFiltros(that);
    await that.setState({
      isLoaded: false
    });
    let filtro = that.state.Users.filter(function (user) {
      let e = user.address.suite.toLowerCase();
      return e.includes('suite') === false;
    });
    await that.setState({
      isLoaded: true,
      Users: filtro
    });
    console.log(that.state.Users);
    $(idBtn).addClass(classBtn);
    Fun.AtivarDataTable();
  }

  async limparFiltros(that) {
    return new Promise(async(resolve)=>{
      await that.setState({
        isLoaded: false
      });
      const Users = await ServicesUser.prototype.getUsers();
      await that.setState({
        isLoaded: true,
        Users
      });
      console.log(that.state.Users);
      Fun.AtivarDataTable();
      resolve(true);
    });
  }

  render() {
    return html(this);
  }
}
export default PageHome;

