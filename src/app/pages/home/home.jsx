import React from 'react';
import { Link } from 'react-router';
import LayoutError from '../../layout/error/error';
import LayoutCarregando from '../../layout/carregando/carregando';

export default function html(that) {

    if (that.state.erro != null) {
        return (<LayoutError erro={that.state.erro}></LayoutError>);
    } else if (!that.state.isLoaded) {
        return (<LayoutCarregando></LayoutCarregando>);
    } else {

        return (
            <div className="animated fadeIn fast">

                <div>
                    <button onClick={that.limparFiltros.bind(this, that)} className="btn btn-default">Limpar Filtros</button>
                    <br/><br/>
                    <button onClick={that.filtroEnderecoComSuite.bind(this, that)} id="btn-endereco-com-suite" className="btn btn-default btn-rounded">Endereços com "suite" </button>
                    <button onClick={that.filtroEnderecoSemSuite.bind(this, that)} id="btn-endereco-sem-suite" className="btn btn-default btn-rounded">Endereços sem "suite"</button>
                    <div className="espaco-top-20"></div>
                </div>

                <table className="table sumir" id="Tabela" data-page-length='15'>
                    <thead className="center">
                        <tr>
                            <th>Nome</th>
                            <th remove-mobile="">Email</th>
                            <th remove-mobile="">Empresa</th>
                            <th>Site</th>
                            <th>Opções</th>
                        </tr>
                    </thead>
                    <tbody>

                        {
                            that.state.Users.map(user => (
                                <tr key={user.id}>
                                    <td>{user.name}</td>
                                    <td remove-mobile="">{user.email}</td>
                                    <td remove-mobile="">{user.company.name}</td>
                                    <td>{user.website}</td>
                                    <td className="center"><Link className="opc verde" to={"/usuario/detalhes/" + user.id}><i className="fa fa-chevron-right" aria-hidden="true"></i></Link></td>
                                </tr>
                            ))
                        }

                    </tbody>
                </table>

            </div>
        )

    }
}