import { request } from '../../../assets/js/fun';
const config = require('../../../config.js');

export class ServicesUser {

  getUsers() {
    return request("GET", config.react.api.baseUrl + '/users', {}, {
      'Content-Type': 'application/json'
  });
  }

}