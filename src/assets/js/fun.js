import $ from 'jquery';
window.jQuery = $;
window.$ = $;
global.jQuery = $;
require('bootstrap');
require('../../../node_modules/datatables.net/js/jquery.dataTables');
require('../../../node_modules/datatables.net-bs/js/dataTables.bootstrap');
require('../../../node_modules/datatables.net-responsive/js/dataTables.responsive');

export function request(Type, Url, Data, Headers = null) {
    return new Promise((resolve, reject) => {
        let body = null;
        if (Type !== "GET") {
            body = JSON.stringify(Data);
        }
        fetch(Url, {
            method: Type,
            body,
            cache: 'no-cache',
            headers: Headers
        }).then(function (res) {
            if (res.ok === true) {
                resolve(res.json());
            } else
                if (res.status === 401) {
                    resolve(res.json());
                } else
                    if (res.statusText != null) {
                        reject(res.status + " " + res.statusText);
                    } else {
                        reject(res.status);
                    }
        }).catch(function (error) {
            reject(error);
            return;
        });

    });
}

export function isJson(item) {
    item = typeof item !== "string"
        ? JSON.stringify(item)
        : item;

    try {
        item = JSON.parse(item);
    } catch (e) {
        return false;
    }

    if (typeof item === "object" && item !== null) {
        return true;
    }

    return false;
}

export function AtivarDataTable() {
    $(document).ready(function () {
        $('.table').css('opacity', '0');
        if ($('#Tabela').hasClass('dataTable')) { } else {
            $('#Tabela').DataTable({
                responsive: false,
                order: [0, 'asc'],
                language: { 'url': '/assets/fw/datatables/languagePT_BR.json' },
                bLengthChange: false
            });
            $('#Tabela').show();
            setInterval(() => {
                $('.table').css('opacity', '0');
                $('#Tabela_filter').css({ 'text-align': 'center' });
                $('#Tabela_wrapper div.col-sm-6').addClass('center');
                $('#Tabela_wrapper div.center').removeClass('col-sm-6');
                $('#Tabela_filter input').attr("placeholder", 'Buscar...');
                $('.table').css('opacity', '1');
                let icon = $('#Tabela_filter label .icon-pesquisa').html();
                if (!icon) {
                    $('#Tabela_filter label').append('<div class="icon-pesquisa"><i class="fa fa-search" aria-hidden="true"></i></div>');
                }
            }, 1);
        }
    });
}

export function titleize(text) {
    var words = text.toLowerCase().split(" ");
    for (var a = 0; a < words.length; a++) {
        var w = words[a];
        words[a] = w[0].toUpperCase() + w.slice(1);
    }
    return words.join(" ");
}
