import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import { Router, Route, browserHistory, IndexRoute } from 'react-router';

import './index.css';
import $ from 'jquery';

import Page404 from './app/pages/404/404';
import PageHome from './app/pages/home/home';
import LayourNavFooter from './app/layout/modelos/nav-footer/reduzido';
import PageDetalhes from './app/pages/detalhes/detalhes';

window.jQuery = $;
window.$ = $;
global.jQuery = $;
require('bootstrap');

const pUrl = process.env.PUBLIC_URL;
const rootElement = document.getElementById('root');
const rootRouters = (
    <Router history={browserHistory}>
        <Route path={pUrl + '/'} name='Home' component={LayourNavFooter}>
            <IndexRoute name='Home' component={PageHome} />
            <Route path={pUrl + 'usuario/detalhes/:id'} name='Detalhes' component={PageDetalhes}/>
        </Route>
        <Route path={pUrl + '/404'} name='404' component={Page404} />
        <Route path={pUrl + '/*'} name='404' component={Page404} />
    </Router>
);
if (rootElement.hasChildNodes()) {
    ReactDOM.hydrate(rootRouters, rootElement);
} else {
    ReactDOM.render(rootRouters, rootElement);
}

// ServicesUserLogin.prototype.verificar();

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
